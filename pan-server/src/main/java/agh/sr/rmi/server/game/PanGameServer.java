package agh.sr.rmi.server.game;

import agh.sr.rmi.client.Bot;
import agh.sr.rmi.client.GameListener;
import agh.sr.rmi.client.PlayerToken;
import agh.sr.rmi.exception.GameException;
import agh.sr.rmi.game.GameServer;
import agh.sr.rmi.game.controller.GameController;
import agh.sr.rmi.server.game.bot.BotFactory;
import agh.sr.rmi.server.game.controller.PanGameController;
import agh.sr.rmi.server.game.player.PanPlayerToken;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Grzegorz Miejski
 *         on 14.04.14.
 */
public class PanGameServer implements GameServer {

    private static final int MAX_NUMBER_OF_PLAYERS = 4;
    private static final int PLAYER_ID_COUNTER = 1;

    private Map<String, GameController> games;

    private Map<String, PlayerToken> players;

    public PanGameServer() {
        games = new HashMap<>();
        players = new HashMap<>();
    }


    @Override
    public GameController createGame(PlayerToken playerToken, String gameName, int playersCount, int botsCount, GameListener gameListener) throws GameException, RemoteException {

        if (!players.containsValue(playerToken)) {
            throw new GameException("No such user registered on server!");
        }

        if (playersCount + botsCount >= MAX_NUMBER_OF_PLAYERS || playersCount + botsCount == 0) {
            throw new GameException("Bad number of players - game must contain 2-4 players!");
        }
        if (games.containsKey(gameName)) {
            throw new GameException("Not unique game name!");
        }

        List<Bot> bots = BotFactory.createBots(gameName, botsCount);

        GameController gameController = new PanGameController(gameName, playersCount, bots);
        UnicastRemoteObject.exportObject(gameController, 0);
        gameController.addPlayer(playerToken, gameListener);

        games.put(gameName, gameController);

        return gameController;
    }

    @Override
    public PlayerToken register(String userName) throws GameException {

        //TODO uncomment
//        if (players.containsKey(userName)) {
//            throw new GameException("Player name already in use!");
//        }
        PlayerToken playerToken = new PanPlayerToken(userName, PLAYER_ID_COUNTER);

        players.put(userName, playerToken);
        return playerToken;
    }

    @Override
    public void unregister(PlayerToken playerToken) throws GameException {
        players.remove(playerToken.getUserName());
    }
}
