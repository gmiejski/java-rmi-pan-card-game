package agh.sr.rmi.server.game.bot;

import agh.sr.rmi.client.Bot;
import agh.sr.rmi.client.PlayerToken;
import agh.sr.rmi.server.game.player.PanPlayerToken;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Grzegorz Miejski
 *         on 14.04.14.
 */
public class BotFactory {

    private static final int BOTS_ID = -1;

    public static List<Bot> createBots(String gameName, int botsCount) {

        List<Bot> result = new ArrayList<>();
        for (int i = 0; i < botsCount; i++) {
            PlayerToken playerToken = new PanPlayerToken(gameName + "_bot_" + i, BOTS_ID);
            BotGameListener botGameListener = new BotGameListener(playerToken);
            Bot bot = new PanBot(playerToken, botGameListener);
            result.add(bot);
        }
        return result;
    }
}
