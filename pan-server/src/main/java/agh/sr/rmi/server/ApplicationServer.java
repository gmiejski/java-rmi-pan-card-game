package agh.sr.rmi.server;

import agh.sr.rmi.game.GameServer;
import agh.sr.rmi.server.game.PanGameServer;
import org.apache.log4j.Logger;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * @author Grzegorz Miejski
 *         on 14.04.14.
 */
public class ApplicationServer {

    private static final Logger logger = Logger.getLogger(ApplicationServer.class);

    private static final String RMI_REGISTRY_ADDRESS = "rmi://127.0.0.1:1099";
    private static final String PAN_GAME_SERVER_REMOTE_OBJECT_NAME = "gameServer";

    public static void main(String[] args) {

        try {
            GameServer gameServer = new PanGameServer();

            UnicastRemoteObject.exportObject(gameServer, 0);

            Registry registry = LocateRegistry.createRegistry(1099);

            Naming.rebind(RMI_REGISTRY_ADDRESS + "/" + PAN_GAME_SERVER_REMOTE_OBJECT_NAME, gameServer);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
