package agh.sr.rmi.server.game.player;

import agh.sr.rmi.client.PlayerToken;

/**
 * @author Grzegorz Miejski
 *         on 14.04.14.
 */
public class PanPlayerToken implements PlayerToken {

    private String userName;
    private Integer id;

    public PanPlayerToken(String userName, Integer id) {
        this.userName = userName;
        this.id = id;
    }

    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PanPlayerToken that = (PanPlayerToken) o;

        if (!id.equals(that.id)) return false;
        if (!userName.equals(that.userName)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userName.hashCode();
        result = 31 * result + id.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "PanPlayerToken{" +
                "userName='" + userName + '\'' +
                ", id=" + id +
                '}';
    }
}
