package agh.sr.rmi.server.game.bot;

import agh.sr.rmi.client.Bot;
import agh.sr.rmi.client.GameListener;
import agh.sr.rmi.client.PlayerToken;
import agh.sr.rmi.client.action.Action;
import agh.sr.rmi.exception.GameException;
import agh.sr.rmi.game.controller.GameController;

import java.rmi.RemoteException;

/**
 * @author Grzegorz Miejski
 *         on 14.04.14.
 */
public class PanBot implements Bot {


    private GameListener botGameListener;
    private PlayerToken botPlayerToken;
    private GameController gameController;

    public PanBot(PlayerToken botPlayerToken, BotGameListener botGameListener) {
        this.botPlayerToken = botPlayerToken;
        this.botGameListener = botGameListener;
    }

    @Override
    public void makeMove(Action action) {

    }

    @Override
    public GameListener getGameListener() {
        return botGameListener;
    }

    @Override
    public PlayerToken getPlayerToken() {
        return botPlayerToken;
    }

    @Override
    public void registerGameController(GameController gameController) throws GameException, RemoteException {
        botGameListener.registerGameController(gameController);
    }
}
