package agh.sr.rmi.server.game.controller;

import agh.sr.rmi.client.Bot;
import agh.sr.rmi.client.GameListener;
import agh.sr.rmi.client.PlayerToken;
import agh.sr.rmi.client.action.Action;
import agh.sr.rmi.exception.GameException;
import agh.sr.rmi.game.controller.GameController;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Grzegorz Miejski
 *         on 14.04.14.
 */
public class PanGameController implements GameController {


    private final int playersCount;
    private final List<GameListener> gameListeners;
    private final List<Bot> bots;
    private final List<PlayerToken> playerTokens;

    private String gameName;
    private Iterator<PlayerToken> turnIterator;

    private boolean running = false;

    public PanGameController(String gameName, int playersCount, List<Bot> bots) throws GameException, RemoteException {

        this.gameListeners = new ArrayList<>();
        this.playerTokens = new ArrayList<>();

        this.gameName = gameName;
        this.playersCount = playersCount;
        this.bots = bots;

        startGameIfReady();

    }

    @Override
    public void makeMove(PlayerToken playerToken, Action action) throws RemoteException {
        System.out.println("Action perforrmed by : " + playerToken);
        informAboutNewTurn();
    }

    private void startGameIfReady() throws RemoteException, GameException {

        if (playerTokens.size() != playersCount) {
            return;
        }

        for (Bot bot : this.bots) {
            gameListeners.add(bot.getGameListener());
            playerTokens.add(bot.getPlayerToken());
            bot.registerGameController(this);
        }

        running = true;

        informAboutNewTurn();

    }

    @Override
    public void addPlayer(PlayerToken playerToken, GameListener gameListener) throws GameException, RemoteException {
        if (playerToken == null) throw new GameException("Null player token");
        playerTokens.add(playerToken);
        gameListeners.add(gameListener);
        gameListener.registerGameController(this);
        startGameIfReady();
    }

    private void informAboutNewTurn() throws RemoteException {
        for (GameListener gameListener : gameListeners) {
            gameListener.newTurn(getNextTurnPlayerToken());
        }
    }

    private PlayerToken getNextTurnPlayerToken() {
        PlayerToken playerToken;
        if (turnIterator == null || !turnIterator.hasNext()) {
            turnIterator = playerTokens.iterator();
        }
        return turnIterator.next();
    }

}
