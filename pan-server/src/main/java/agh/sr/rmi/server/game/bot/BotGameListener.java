package agh.sr.rmi.server.game.bot;

import agh.sr.rmi.client.GameListener;
import agh.sr.rmi.client.PlayerToken;
import agh.sr.rmi.exception.GameException;
import agh.sr.rmi.game.controller.GameController;

import java.rmi.RemoteException;

/**
 * @author Grzegorz Miejski
 *         on 14.04.14.
 */
public class BotGameListener implements GameListener {

    private GameController botGameController;
    private PlayerToken playerToken;

    public BotGameListener(PlayerToken playerToken) {
        this.playerToken = playerToken;
    }

    @Override
    public void newTurn(PlayerToken playerToken) throws RemoteException {
        playerToken.getId();
    }

    @Override
    public void registerGameController(GameController gameController) throws GameException, RemoteException {
        if (botGameController != null) {
            throw new GameException("This bot already has a game listener registered!");
        }
        if (gameController == null) {
            throw new GameException("empty bot game listener");
        }
        this.botGameController = gameController;
    }
}
