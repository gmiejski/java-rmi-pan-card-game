package agh.sr.rmi.client;

import agh.sr.rmi.client.listener.ClientGameListener;
import agh.sr.rmi.game.GameServer;
import agh.sr.rmi.game.controller.GameController;
import org.apache.log4j.Logger;

import java.rmi.Naming;
import java.rmi.server.UnicastRemoteObject;

/**
 * @author Grzegorz Miejski
 *         on 14.04.14.
 */
public class Client {

    private static final Logger logger = Logger.getLogger(Client.class);

    private static final String RMI_REGISTRY_ADDRESS = "rmi://127.0.0.1:1099";
    private static final String PAN_GAME_SERVER_REMOTE_OBJECT_NAME = "gameServer";

    public static void main(String[] args) {

        try {

            if (System.getSecurityManager() == null) {
                System.setSecurityManager(new SecurityManager());
            }


//            System.out.println("Enter your nick:");
//            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
//            String nick = bufferedReader.readLine();
            GameServer gameServer = (GameServer) Naming
                    .lookup(RMI_REGISTRY_ADDRESS + "/" + PAN_GAME_SERVER_REMOTE_OBJECT_NAME);

            String userName = "g1";

            PlayerToken playerToken = gameServer.register(userName);

            GameListener gameListener = new ClientGameListener(playerToken);

            UnicastRemoteObject.exportObject(gameListener, 0);

            GameController gameController = gameServer.createGame(playerToken, "game1", 1, 1, gameListener);
            gameListener.registerGameController(gameController);

//            System.out.println("Aktualna zawartosc tablicy: " + noteboard.getText(userToken));

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

}
