package agh.sr.rmi.client.listener;

import agh.sr.rmi.client.GameListener;
import agh.sr.rmi.client.PlayerToken;
import agh.sr.rmi.client.action.Action;
import agh.sr.rmi.game.controller.GameController;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.rmi.RemoteException;

/**
 * @author Grzegorz Miejski
 *         on 14.04.14.
 */
public class ClientGameListener implements GameListener {


    private BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    private PlayerToken currentPlayerToken;
    private GameController gameController;

    public ClientGameListener(PlayerToken currentPlayerToken, GameController gameController) {
        this.currentPlayerToken = currentPlayerToken;
        this.gameController = gameController;
    }

    public ClientGameListener(PlayerToken playerToken) {
        this.currentPlayerToken = playerToken;
    }

    @Override
    public void newTurn(PlayerToken playerToken) {
        System.out.println("New turn for player : " + playerToken);
        if (playerToken.equals(currentPlayerToken)) {
            System.out.println("Its your turn! Sending action to server!");
            try {
                gameController.makeMove(playerToken, new Action());
            } catch (RemoteException e) {
                System.out.println("Making new move gone so wrong!");
                e.printStackTrace();
            }
        }
    }

    @Override
    public void registerGameController(GameController gameController) {
        if (this.gameController == null) {
            this.gameController = gameController;
        }
    }
}
