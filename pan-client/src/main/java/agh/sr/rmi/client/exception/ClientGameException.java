package agh.sr.rmi.client.exception;

import agh.sr.rmi.exception.GameException;

/**
 * @author Grzegorz Miejski
 *         on 14.04.14.
 */
public class ClientGameException extends GameException {

    public ClientGameException() {
    }

    public ClientGameException(String message) {
        super(message);
    }
}
