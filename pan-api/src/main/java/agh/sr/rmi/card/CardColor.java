package agh.sr.rmi.card;

/**
 * @author Grzegorz Miejski
 *         on 14.04.14.
 */
public enum CardColor {
    CLUBS, DIAMONDS, HEARTS, SPADES
}
