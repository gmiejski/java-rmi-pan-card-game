package agh.sr.rmi.card;

/**
 * @author Grzegorz Miejski
 *         on 14.04.14.
 */
public enum CardValue {
    NINE, TEN, JACK, QUEEN, KING, ACE
}
