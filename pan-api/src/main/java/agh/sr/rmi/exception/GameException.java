package agh.sr.rmi.exception;

/**
 * @author Grzegorz Miejski
 *         on 14.04.14.
 */
public class GameException extends Exception {

    public GameException() {
    }

    public GameException(String message) {
        super(message);
    }
}
