package agh.sr.rmi.game;

import agh.sr.rmi.client.GameListener;
import agh.sr.rmi.client.PlayerToken;
import agh.sr.rmi.exception.GameException;
import agh.sr.rmi.game.controller.GameController;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Grzegorz Miejski
 *         on 14.04.14.
 */
public interface GameServer extends Remote {

    GameController createGame(PlayerToken playerToken, String gameName, int players, int bots, GameListener gameListener) throws GameException, RemoteException;


    PlayerToken register(String userName) throws GameException, RemoteException;

    void unregister(PlayerToken playerToken) throws GameException, RemoteException;
}
