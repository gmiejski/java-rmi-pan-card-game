package agh.sr.rmi.game.controller;

import agh.sr.rmi.client.GameListener;
import agh.sr.rmi.client.PlayerToken;
import agh.sr.rmi.client.action.Action;
import agh.sr.rmi.exception.GameException;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Grzegorz Miejski
 *         on 14.04.14.
 */
public interface GameController extends Remote {

    void makeMove(PlayerToken playerToken, Action action) throws RemoteException;

    void addPlayer(PlayerToken playerToken, GameListener gameListener) throws GameException, RemoteException;

}
