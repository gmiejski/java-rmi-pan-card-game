package agh.sr.rmi.client;

import java.io.Serializable;

/**
 * @author Grzegorz Miejski
 *         on 14.04.14.
 */
public interface PlayerToken extends Serializable {
    String getUserName();
    Integer getId();
}
