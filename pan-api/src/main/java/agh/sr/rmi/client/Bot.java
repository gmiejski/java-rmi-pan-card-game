package agh.sr.rmi.client;

import agh.sr.rmi.client.action.Action;
import agh.sr.rmi.exception.GameException;
import agh.sr.rmi.game.controller.GameController;

import java.rmi.RemoteException;

/**
 * @author Grzegorz Miejski
 *         on 14.04.14.
 */
public interface Bot {

    void makeMove(Action action);

    GameListener getGameListener();

    PlayerToken getPlayerToken();

    void registerGameController(GameController gameController) throws GameException, RemoteException;
}
