package agh.sr.rmi.client;

import agh.sr.rmi.exception.GameException;
import agh.sr.rmi.game.controller.GameController;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Grzegorz Miejski
 *         on 14.04.14.
 */
public interface GameListener extends Remote {

    void newTurn(PlayerToken playerToken) throws RemoteException;

    void registerGameController(GameController gameController) throws GameException, RemoteException;
}
